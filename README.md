# ut3-survival
Set of tools to stay sane while teaching at UT3.

### Rationale
Many tools are available at UT3 to get various information.
However, most of these tools have not been made with automation in mind either because their interface is only interactive (graphical) or because the output formats are not usable (looking at you xls files), or both.

Tools from this repo aims at helping the automation of tedious stuff that we regularly have to do, such as:
- Who are the students I have class with?
- Can I get the Moodle submissions of my students?

### Acknowledgements
- The REALIST conversion script (XLS -> CSV student list) is heavily inspired from a script written by Emmanuel Rio.
