{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/22.11.tar.gz";
    sha256 = "sha256:11w3wn2yjhaa5pv20gbfbirvjq6i3m7pqrq2msf0g7cv44vijwgw";
  }) {}
}:

let
  pyPkgs = pkgs.python3Packages;
in rec {
  ut3_survival = pyPkgs.buildPythonPackage {
    pname = "ut3_survival";
    version = "local";
    format = "pyproject";

    src = pkgs.lib.sourceByRegex ./. [
      "pyproject\.toml"
      "LICENSE"
      "ut3_survival"
      "ut3_survival/.*\.py"
      "ut3_survival/cmd"
      "ut3_survival/cmd/.*\.py"
    ];
    buildInputs = with pyPkgs; [
      flit
    ];
    propagatedBuildInputs = with pyPkgs; [
      xlrd
      pandas
      patool
      click
    ];
  };

  user-shell = pkgs.mkShell {
    buildInputs = with pyPkgs; [
      ipython
      ut3_survival
    ];
  };

  dev-shell = pkgs.mkShell {
    buildInputs = with pyPkgs; [
      ipython
    ] ++ ut3_survival.propagatedBuildInputs;
  };
}
