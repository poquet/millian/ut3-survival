#!/usr/bin/env python3
import sys

import click
import pandas

from ut3_survival import realist
from ut3_survival import moodle

@click.command()
@click.option('-z', '--moodle-submissions-zip-file', required=True, help='The zip file that contains all the submissions done by the students.')
@click.option('-p', '--moodle-participants-csv-file', required=True, help='The csv file that lists the participants of the Moodle course you evaluate.')
@click.option('-s', '--realist-students-csv-file', required=True, help='The csv file that lists the students that you have to evaluate.')
@click.option('-o', '--output-dir', required=True, help='The output directory where the assessment should be prepared.')
@click.option('-x', '--extract', is_flag=True, default=False, help='If set, extract archived files into each student directory.')
def main(moodle_submissions_zip_file, moodle_participants_csv_file, realist_students_csv_file, output_dir, extract):
    students = realist.read_parse_csv(realist_students_csv_file)
    moodle_participants = moodle.read_parse_participants(moodle_participants_csv_file)
    students_to_keep = moodle.join_participants_with_realist_students(moodle_participants, students)

    moodle.prepare_assessment_repo(moodle_submissions_zip_file, output_dir, students_to_keep)

    if extract:
        moodle.extract_archives_from_repo(output_dir)

if __name__ == "__main__":
    main()
