#!/usr/bin/env python3
import sys

import click
import pandas

from ut3_survival import realist

@click.command()
@click.argument('xls_file', required=True, nargs=-1)
@click.option('-o', '--output', default=None, help='If set, parsed students are written as CSV to this file.')
@click.option('--lower', is_flag=True, show_default=True, default=False, help='Sets text data to lowercase.')
def main(xls_file, output, lower):
    students = realist.read_parse_several_xls(xls_file, lower)

    output_file = sys.stdout
    if output is not None:
        output_file = open(output, 'wt', encoding='utf-8')

    students_df = realist.student_entry_list_to_df(students)
    students_df.sort_values(by=['group', 'lastname', 'firstname', 'id'], inplace=True)
    students_df.to_csv(output_file, index=False)

if __name__ == "__main__":
    main()
