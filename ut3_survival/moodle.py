#!/usr/bin/env python3
import glob
import os
import re
import shutil
import sys
import tempfile
import zipfile

import pandas
import patoolib

def read_parse_participants(filename: str) -> pandas.DataFrame:
    df = pandas.read_csv(filename)

    column_names = [str(x) for x in df.columns]
    expected_column_names = ['Prénom', 'Nom de famille', "Numéro d’identification", 'Adresse de courriel']

    missing_cols = list()
    for col_name in expected_column_names:
        if col_name not in column_names:
            missing_cols.append(col_name)

    if len(missing_cols) > 0:
        raise RuntimeError(f"missing column names in moodle participant file '{filename}': '{missing_cols}'. got '{column_names}' while '{expected_column_names}' was expected")

    df.rename(columns={
        expected_column_names[0]: "moodle_firstname",
        expected_column_names[1]: "moodle_lastname",
        expected_column_names[2]: "id",
        expected_column_names[3]: "moodle_email",
    }, inplace=True)
    df['id'] = df['id'].fillna(-1)
    df = df.astype({"id":int})
    return df

def join_participants_with_realist_students(participants_df, realist_students_df, check=True):
    joined_df = realist_students_df.merge(participants_df, how='inner')

    if len(realist_students_df) != len(joined_df):
        error_msg = 'some students have been lost by the inner join of realist students to moodle participants!'
        print(error_msg, file=sys.stderr)
        if check:
            raise ValueError(error_msg)

    return joined_df

def name_to_dirname(input_name: str) -> str:
    return input_name.strip().lower().replace(" ", "_")

def prepare_assessment_repo(submissions_zip_filename: str, repo_path: str, students_to_keep_df: pandas.DataFrame, orig_dirname='.orig'):
    with tempfile.TemporaryDirectory() as tmp_extract_dir:
        with zipfile.ZipFile(submissions_zip_filename, 'r') as zf:
            zf.extractall(path=tmp_extract_dir)

        # parse the name of the directories in the zip extract
        regex = re.compile('^(.* .*)_\d+.*$')
        prefix_to_dirname = {}
        subdir_names = {x.name for x in os.scandir(tmp_extract_dir)}
        for subdir_name in subdir_names:
            m = regex.match(subdir_name)
            if m is None:
                print(f"directory '{subdir_name}' could not be parsed", file=sys.stderr)
                continue
            if m.group(1) in prefix_to_dirname:
                print(f"duplication of prefix '{m.group(1)}' while parsing directories of zipfile '{submissions_zip_filename}'")
                continue
            prefix_to_dirname[m.group(1)] = m.group(0)

        os.makedirs(repo_path)
        for index, student in students_to_keep_df.iterrows():
            expected_dir_prefix = f"{student['moodle_lastname']} {student['moodle_firstname']}"

            if expected_dir_prefix not in prefix_to_dirname:
                print(f"warning: '{expected_dir_prefix}' dir not found in zip. student: {dict(student[['id', 'lastname', 'firstname', 'email']])}", file=sys.stderr)
                continue

            renamed_dir = "{}/{}-{}-{}/{}".format(
                repo_path,
                name_to_dirname(student['moodle_lastname']),
                name_to_dirname(student['moodle_firstname']),
                student['id'],
                orig_dirname,
            )

            shutil.move("/".join([tmp_extract_dir, prefix_to_dirname[expected_dir_prefix]]), renamed_dir)


def extract_archives_from_repo(repo_path, orig_dirname='.orig'):
    for orig_dir in glob.glob(f"{repo_path}/*/{orig_dirname}"):
        student_assessment_dir = orig_dir + '/../'
        for file in os.scandir(orig_dir):
            if file.is_file():
                try:
                    #print(f"{file.path}, {orig_dir}")
                    patoolib.extract_archive(file.path, outdir=student_assessment_dir, interactive=False, verbosity=-1)
                except patoolib.util.PatoolError:
                    shutil.move(file.path, student_assessment_dir)
