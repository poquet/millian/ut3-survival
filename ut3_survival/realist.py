#!/usr/bin/env python3
'''
Student list parser from REALIST (usually accessed via SGCE).
'''

import sys
from collections import namedtuple

import pandas
import xlrd

student_columns = ['id', 'lastname', 'firstname', 'email', 'group']
StudentEntry = namedtuple('StudentEntry', student_columns)

def read_parse_xls(xls_filename: str, lower: bool=None) -> [StudentEntry]:
    '''Read and parse a XLS file into a list of students.'''
    set_lowercase = False
    if lower is not None:
        if not isinstance(lower, bool):
            raise TypeError(f'lower is not a bool (type=type{type(lower)})')
        set_lowercase = lower

    students_xls = xlrd.open_workbook(xls_filename, logfile=sys.stderr)
    group_names = students_xls.sheet_names()
    group_names.pop(0) # first XLS sheet is useless

    students = []
    for group_name in group_names:
        sheet = students_xls.sheet_by_name(group_name)
        if sheet.cell_value(1,0) != "GROUPE : " + group_name:
            raise AssertionError("SGCE's xls format has changed")

        for row in range(3, sheet.nrows):
            entry = StudentEntry(
                id=sheet.cell_value(row,0),
                lastname=sheet.cell_value(row,1),
                firstname=sheet.cell_value(row,2),
                email=sheet.cell_value(row,3),
                group=group_name,
            )

            if set_lowercase:
                entry = StudentEntry(
                    id=entry.id,
                    lastname=entry.lastname.lower(),
                    firstname=entry.firstname.lower(),
                    email=entry.email.lower(),
                    group=entry.group.lower(),
                )

            students.append(entry)
    return students

def read_parse_several_xls(xls_filenames: [str], lower: bool=None) -> [StudentEntry]:
    '''read_parse_xls wrapper when several files are to be used.'''
    all_students = []
    for xls_filename in xls_filenames:
        try:
            students = read_parse_xls(xls_filename, lower)
            all_students.extend(students)
        except Exception as exception:
            raise RuntimeError(f"could not read/parse xls file '{xls_filename}'") from exception
    return all_students

def student_entry_list_to_df(students: [StudentEntry]) -> pandas.DataFrame:
    '''Create a DataFrame from a student list.'''
    return pandas.DataFrame(students, columns=student_columns)

def read_parse_csv(csv_filename: str) -> pandas.DataFrame:
    df = pandas.read_csv(csv_filename)

    expected_columns = set(student_columns)
    parsed_columns = {str(x) for x in df.columns}
    if not expected_columns.issubset(parsed_columns):
        raise RuntimeError(f"missing columns in csv file '{csv_filename}': {expected_columns - parsed_columns}")

    return df
